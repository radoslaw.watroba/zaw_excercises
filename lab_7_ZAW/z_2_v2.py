import matplotlib.pyplot as plt
import cv2
import numpy as np
from scipy.spatial import distance
import scipy
from scipy.optimize import minimize
import os
#from scipy.spatial.distance import directed_hausdorff

def xy_contour (contours, nr):
    contours = contours[nr]
    xy = contours[:, 0, :]
    x = contours[:, 0, 0]
    y = contours[:, 0, 1]

    M = cv2.moments(contours, 1)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    x = x - cx
    y = y - cy
    xy[:, 0] = x
    xy[:, 1] = y
    # xy[0] = xy[0] - cx
    # xy[1] = xy[1] - cy

    max_dist = 0
    for i in xy:
        for j in xy:
            dist = distance.euclidean(i, j)
            if dist > max_dist:
                max_dist = dist
    x = x / max_dist
    y = y / max_dist

    xy = xy.astype(np.float)
    xy[:, 0] = x
    xy[:, 1] = y
    # xy = xy/max_dist
    return xy, cx, cy

def dH(xy_fig1, xy_fig2, fi):
    dst = []
    for i in xy_fig1:
        dst_point = []
        for j in xy_fig2:
            dst_point.append(distance.euclidean(i, j))
        max_dst_point = min(dst_point)
        dst.append(max_dst_point)
    max_dst = max(dst)
    return max_dst


def hausdorf(xy_1, xy_2, fi):
    nx1 = xy_2[:, 0] * np.cos(fi) - xy_2[:, 1] * np.sin(fi)
    ny1 = xy_2[:, 0] * np.sin(fi) + xy_2[:, 1] * np.cos(fi)
    xy_2[:, 0] = nx1
    xy_2[:, 1] = ny1
    dH1 = dH(xy_1, xy_2, fi)
    dH2 = dH(xy_2, xy_1, fi)
    return max(dH1, dH2)

def hausdorf_2(fi, x_1, y_1, x_2, y_2):
    xy_1 = np.ndarray(shape=(len(x_1), 2), dtype=float, order='F')
    xy_2 = np.ndarray(shape=(len(x_2), 2), dtype=float, order='F')
    xy_1[:, 0] = x_1
    xy_1[:, 1] = y_1
    xy_2[:, 0] = x_2
    xy_2[:, 1] = y_2
    nx1 = xy_2[:, 0] * np.cos(fi) - xy_2[:, 1] * np.sin(fi)
    ny1 = xy_2[:, 0] * np.sin(fi) + xy_2[:, 1] * np.cos(fi)
    xy_2[:, 0] = nx1
    xy_2[:, 1] = ny1
    dH1 = dH(xy_1, xy_2, fi)
    dH2 = dH(xy_2, xy_1, fi)
    return max(dH1, dH2)

I = cv2.imread('ithaca_query.bmp')
I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
I_prev = I
I = ~I

thresh = cv2.threshold(I, 0, 255, cv2.THRESH_BINARY)
thresh = thresh[1]
#ret, thresh = cv2.threshold(I, 127, 255, 0)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

xy_I, cx_I, cy_I = xy_contour(contours, 0)

imgs = os.listdir('imgs_2')
dist_all = []
fi_all = []
for l in imgs:
    nazwa_ze_sciezka = 'imgs_2/' + l
    J = cv2.imread(nazwa_ze_sciezka)
    J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
    J = ~J

    ret, thresh = cv2.threshold(J, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    xy_J, cx_J, cy_J = xy_contour(contours, 0)

    katMin = np.zeros(12)
    dMin = np.zeros(12)
    for i in range(12):
        #i = i + 1 #usun
        x_I = xy_I[:, 0]
        y_I = xy_I[:, 1]
        x_J = xy_J[:, 0]
        y_J = xy_J[:, 1]
        minimum = scipy.optimize.fmin(hausdorf_2, np.deg2rad(30*i), (x_I, y_I, x_J, y_J)) #no źle skurwol działa, nie wiem czemu
        print(minimum)
        katMin[i] = minimum
        print(katMin)
        dMin[i] = hausdorf(xy_I, xy_J, katMin[i])  # hausdorff to przykladowa nazwa naszej funkcji liczacej odleglosc Hausdorffa z osobnymi tablicami dla wspolrzednych x i y kazdego z konturow
        print(dMin)
    hausd = min(dMin)
    min_idx = dMin.argmin()
    fi = katMin[min_idx]
    fi_all.append(fi)
    dist_all.append(hausd)
    print(l, '. ', hausd)
    #dist_all.append(max(directed_hausdorff(xy_I, xy_J)[0], directed_hausdorff(xy_J, xy_I)[0]))

print(imgs[np.argmin(dist_all)], fi_all[np.argmin(dist_all)])

nazwa_ze_sciezka = 'imgs_2/' + imgs[np.argmin(dist_all)]
J = cv2.imread(nazwa_ze_sciezka)
J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
J = ~J

ret, thresh = cv2.threshold(J, 127, 255, 0)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

xy_J_min, cx_J_min, cy_J_min = xy_contour(contours, 0)

fi = fi_all[np.argmin(dist_all)]
nx1 = xy_J_min[:, 0] * np.cos(fi) - xy_J_min[:, 1] * np.sin(fi)
ny1 = xy_J_min[:, 0] * np.sin(fi) + xy_J_min[:, 1] * np.cos(fi)
xy_J_min[:, 0] = nx1
xy_J_min[:, 1] = ny1

fig = plt.figure()
ax = fig.add_subplot(111)
ax.plot(xy_I[:, 0], xy_I[:, 1], 'b')
ax.plot(xy_J_min[:, 0], xy_J_min[:, 1], 'r')

plt.savefig('zadanie_2_v2.png')
plt.show()
