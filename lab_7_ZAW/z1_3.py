import matplotlib.pyplot as plt
import cv2
import numpy as np
from scipy.spatial import distance
import os

def xy_contour (contours, nr):
    contours = contours[nr]
    xy = contours[:, 0, :]
    x = contours[:, 0, 0]
    y = contours[:, 0, 1]

    M = cv2.moments(contours, 1)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    x = x - cx
    y = y - cy
    xy[:, 0] = x
    xy[:, 1] = y
    # xy[0] = xy[0] - cx
    # xy[1] = xy[1] - cy

    max_dist = 0
    for i in xy:
        for j in xy:
            dist = distance.euclidean(i, j)
            if dist > max_dist:
                max_dist = dist
    x = x / max_dist
    y = y / max_dist

    xy = xy.astype(np.float)
    xy[:, 0] = x
    xy[:, 1] = y
    # xy = xy/max_dist
    return xy, cx, cy

def dH(xy_fig1, xy_fig2):
    dst = []
    for i in xy_fig1:
        dst_point = []
        for j in xy_fig2:
            dst_point.append(distance.euclidean(i, j))
        max_dst_point = min(dst_point)
        dst.append(max_dst_point)
    max_dst = max(dst)
    return max_dst



# def dH(xy_1, xy_2):
#     max_dist = 0
#     min_dist = 1000000000000000
#     #szukam najpierw punktu najbliższego zbiorowi A w zbiorze B
#     for j in range(0, xy_2.shape[0]):
#         for i in range(0, xy_1.shape[0]):
#             m_dist = np.linalg.norm(xy_2[j] - xy_1[i])
#             if m_dist < min_dist:
#                 min_dist = m_dist
#                 min_B = xy_2[j]
#     for i in range(0, xy_1.shape[0]):
#         dist = np.linalg.norm(xy_1[i] - min_B)
#         if dist > max_dist:
#             max_dist = dist
#     return max_dist
#
#
def hausdorf(xy_1, xy_2):
    return max(dH(xy_1, xy_2), dH(xy_2, xy_1))

I = cv2.imread('Aegeansea.jpg')
IHSV = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)

plt.figure()
plt.imshow(I)
plt.title('Obrazek :)')

IH = IHSV[:, :, 0]
IS = IHSV[:, :, 1]
IV = IHSV[:, :, 2]

thresh_IS = cv2.threshold(IS, 30, 255, cv2.THRESH_BINARY)
thresh_IS = thresh_IS[1]

IH = ~IH
thresh_IH = cv2.threshold(IH, 255-60, 255, cv2.THRESH_BINARY)
thresh_IH = thresh_IH[1]

map = thresh_IH & thresh_IS
map_prev = map
plt.figure()
plt.imshow(map)
plt.title('Map')
plt.gray()
plt.show()

contours, hierarchy = cv2.findContours(map, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
contours = list(filter(lambda el: el.shape[0] > 60 and el.shape[0] < 3000, contours))
#contours = [el for el in contours if el.shape[0] > 15 and el.shape[0] < 3000]


height, width = I.shape[:2]
scale = 0.5
I = cv2.resize(I, (int(scale*height), int(scale*width)))

cv2.imwrite('Mapa_cala.png', I)
xy_I_list = []
cx_I_list = []
cy_I_list = []
for l in range(0, len(contours)):
    xy_I, cx_I, cy_I = xy_contour(contours, l)
    xy_I_list.append(xy_I)
    cx_I_list.append(cx_I)
    cy_I_list.append(cy_I)
    cv2.putText(I, str(l), (int(cx_I / 2), int(cy_I / 2)), cv2.FONT_HERSHEY_SIMPLEX, 1, (128, 128, 128))


imgs = os.listdir('imgs')
for k in imgs:
    nazwa_ze_sciezka = 'imgs/' + k
    I_ref = cv2.imread(nazwa_ze_sciezka)
    I_ref = cv2.cvtColor(I_ref, cv2.COLOR_BGR2GRAY)
    # kernel = np.ones((3, 3), np.uint8)
    # I_ref = cv2.erode(I_ref, kernel, iterations=1)
    # cv2.imshow("Binary1", I_ref)
    I_ref = ~I_ref
    thresh_ref = cv2.threshold(I_ref, 0, 255, cv2.THRESH_BINARY)
    thresh_ref = thresh_ref[1]
    contours_ref, hierarchy_ref = cv2.findContours(thresh_ref, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    xy_I_ref, cx_I_ref, cy_I_ref = xy_contour(contours_ref, 0)
    dist_all = []
    for l in range(0, len(contours)):

        if l > 0 and hausdorf(xy_I_list[l], xy_I_ref) < min(dist_all):
            cx_I_best = cx_I_list[l]
            cy_I_best = cy_I_list[l]
            if hausdorf(xy_I_list[l], xy_I_ref) < 0.15:
                wypisz = 1
            else:
                wypisz = 0

        dist_all.append(hausdorf(xy_I_list[l], xy_I_ref))
        print(k, '.', l, '.', hausdorf(xy_I_list[l], xy_I_ref))
        print(int(cx_I_list[l]/2), int(cy_I_list[l]/2))
        #dist_all.append(max(directed_hausdorff(xy_I, xy_J)[0], directed_hausdorff(xy_J, xy_I)[0]))

    island = np.argmin(dist_all)
    print(island)
    #xy_I_best, cx_I_best, cy_I_best = xy_contour(contours, 9)
    if wypisz == 1:
        nazwa = k.split('.')[0].split('_')[1]
        cv2.putText(I, nazwa, (int(cx_I_best/2), int(cy_I_best/2)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255))

# plt.figure()
# plt.imshow(map)
# plt.title('Kontur :)')
# plt.show()

#uwaga! program robi tak, ze napisy sa na siebie nalozone, zeby temu zapobiec trzeba odwrocic petle, tzn. najpierw
#wybieramy kontur na mapie a potem szukamy dopasowania w folderze
#uwaga!2 podobno da sie to zrobić tak, żeby program wykonał się w godzine, półtora, zamiast 4-5, "trzeba umieć operować na macierzach"
#uwaga!3 błędy w działaniu mogą wynikać z faktu, że w folderze z wyspami są źle poobracane, warto poświęcić chwile i je poobracać, żeby nie robić tego programu 2 razy, bo szkoda czasu

cv2.imshow('result', I)
cv2.imwrite('Mapa_cala.png', I)
# cv2.resizeWindow('result', (int(I.shape[0]/2), int(I.shape[1]/2)))
cv2.waitKey(0)