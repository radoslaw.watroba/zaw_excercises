import matplotlib.pyplot as plt
import cv2
import numpy as np
from scipy.spatial import distance
import os
#from scipy.spatial.distance import directed_hausdorff

def xy_contour (contours, nr):
    contours = contours[nr]
    xy = contours[:, 0, :]
    x = contours[:, 0, 0]
    y = contours[:, 0, 1]

    M = cv2.moments(contours, 1)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    x = x - cx
    y = y - cy
    xy[:, 0] = x
    xy[:, 1] = y
    # xy[0] = xy[0] - cx
    # xy[1] = xy[1] - cy

    max_dist = 0
    for i in xy:
        for j in xy:
            dist = distance.euclidean(i, j)
            if dist > max_dist:
                max_dist = dist
    x = x / max_dist
    y = y / max_dist

    xy = xy.astype(np.float)
    xy[:, 0] = x
    xy[:, 1] = y
    # xy = xy/max_dist
    return xy, cx, cy

def dH(xy_fig1, xy_fig2):
    dst = []
    for i in xy_fig1:
        dst_point = []
        for j in xy_fig2:
            dst_point.append(distance.euclidean(i, j))
        max_dst_point = min(dst_point)
        dst.append(max_dst_point)
    max_dst = max(dst)
    return max_dst


def hausdorf(xy_1, xy_2):
    return max(dH(xy_1, xy_2), dH(xy_2, xy_1))

I = cv2.imread('ithaca_q.bmp')
I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
I_prev = I
I = ~I
plt.figure()
plt.gray()
plt.imshow(I)
plt.title('Obrazek :)')

thresh = cv2.threshold(I, 0, 255, cv2.THRESH_BINARY)
thresh = thresh[1]
#ret, thresh = cv2.threshold(I, 127, 255, 0)
contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
color = (0, 255, 0)
image = np.zeros((I.shape[0], I.shape[1], 1), np.uint8)
cv2.drawContours(I, contours, 0, color)
contour = cv2.absdiff(I, I_prev)
plt.figure()
plt.imshow(contour)
plt.title('Kontur :)')
plt.show()

xy_I, cx_I, cy_I = xy_contour(contours, 0)

imgs = os.listdir('imgs')
dist_all = []
for l in imgs:
    nazwa_ze_sciezka = 'imgs/' + l
    J = cv2.imread(nazwa_ze_sciezka)
    J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
    J = ~J

    ret, thresh = cv2.threshold(J, 127, 255, 0)
    contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    xy_J, cx_J, cy_J = xy_contour(contours, 0)
    dist_all.append(hausdorf(xy_I, xy_J))
    print(l, '. ', hausdorf(xy_I, xy_J))
    #dist_all.append(max(directed_hausdorff(xy_I, xy_J)[0], directed_hausdorff(xy_J, xy_I)[0]))

print(imgs[np.argmin(dist_all)])