import cv2
import numpy as np
import matplotlib.pyplot as plt

I = cv2.imread('I.jpg')
I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
J = cv2.imread('J.jpg')
J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)

cv2.imshow("I.jpg", I)
cv2.imshow("J.jpg", J)

Diff = cv2.absdiff(I, J)
cv2.imshow("Difference", Diff)

cv2.waitKey(10)

#P = np.ones(3, 3)
W2 = 1
dX = 1
dY = 1
dist = []

YY, XX = I.shape[:2] #height, width

u = np.zeros((YY, XX))
v = np.zeros((YY, XX))

for j in range(W2+1, YY-W2-1):
    for i in range(W2+1, XX-W2-1):
        IO = np.float32(I[j - W2:j + W2 + 1, i - W2:i + W2 + 1])
        min_dist = 10000000
        for j_1 in range(j - dY, j + dY + 1):
            for i_1 in range(i - dX, i + dX + 1):
                JO = np.float32(J[j_1 - W2:j_1 + W2 + 1, i_1 - W2:i_1 + W2 + 1])
                dist = np.sum(np.sqrt((np.square(JO - IO)))) #sprobuje kiedys absdiffa
                if (dist < min_dist):
                    min_dist = dist
                    u[j, i] = j_1 - j
                    v[j, i] = i_1 - i
plt.quiver(u, v)
plt.gca().invert_yaxis()
plt.show()
















