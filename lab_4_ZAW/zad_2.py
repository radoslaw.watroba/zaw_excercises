import cv2
import numpy as np
import matplotlib.pyplot as plt


def pyramid(im, max_scale):
    images = [im]
    for k in range(1, max_scale):
        images.append(cv2.resize(images[k-1], (0,0), fx=0.5, fy=0.5))
    return images


def of(I, J, u0, v0, W2=1, dY=1, dX=1):
    YY, XX = I.shape[:2]  # height, width
    u1 = np.zeros((YY, XX))
    v1 = np.zeros((YY, XX))
    for j in range(W2+1, YY-W2-1):
        for i in range(W2+1, XX-W2-1):
            IO = np.float32(I[j - W2:j + W2 + 1, i - W2:i + W2 + 1])
            min_dist = 10000000
            for j_1 in range(j - dY, j + dY + 1):
                for i_1 in range(i - dX, i + dX + 1):
                    j_1 = int(j_1 - u0[j, i]) #albo to zakomentuj i odkomentuj na dole
                    i_1 = int(i_1 - v0[j, i])
                    if j_1 < (YY - W2) and i_1 < (XX - W2) and i_1 > W2 and j_1 > W2:
                        JO = np.float32(J[j_1 - W2:j_1 + W2 + 1, i_1 - W2:i_1 + W2 + 1])
                        #JO = np.float32(J[j_1 - int(u0[j, i]) - W2:j_1 - int(u0[j, i]) + W2 + 1,
                        #                i_1 - int(v0[j, i]) - W2:i_1 - int(v0[j, i]) + W2 + 1])
                        dist = np.sum(np.sqrt((np.square(JO - IO)))) #sprobuje kiedys absdiffa
                        if (dist < min_dist):
                            min_dist = dist
                            u1[j, i] = j_1 - j # -u0[j, i]
                            v1[j, i] = i_1 - i # -v0[j, i]
    return u1, v1

I = cv2.imread('I.jpg')
I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
J = cv2.imread('J.jpg')
J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)

cv2.imshow("I.jpg", I)
cv2.imshow("J.jpg", J)

Diff = cv2.absdiff(I, J)
cv2.imshow("Difference", Diff)

cv2.waitKey(10)

# YY, XX = I.shape[:2]  # height, width
# u = np.zeros((YY, XX))
# v = np.zeros((YY, XX))

K = 3 #ilosc szkalowan

IP = pyramid(I, K)
JP = pyramid(J, K)

u0 = np.zeros(IP[-1].shape, np.float32) #wektor[-1] <- dostep do ostatniego elementu
v0 = np.zeros(JP[-1].shape, np.float32)

u1, v1 = of(IP[-1], JP[-1], u0, v0)
for k in range(1, K):
    v1 = cv2.resize(v1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
    u1 = cv2.resize(u1, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
    u1, v1 = of(IP[-k-1], JP[-k-1], u1, v1)


plt.quiver(u1, v1)
plt.gca().invert_yaxis()
plt.show()