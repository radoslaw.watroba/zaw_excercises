import cv2
import numpy as np

f = open('temporalROI.txt', 'r')  # otwarcie pliku
line = f.readline()  # odczyt lini
roi_start, roi_end = line.split()  # rozbicie lini na poszczegolne framgenty tesktu
roi_start = int(roi_start)  # konwersja na int
roi_end = int(roi_end)  # konwersja na int

step = 1

I_VIS = cv2.imread('input/in%06d.jpg' % roi_start)
YY, XX = I_VIS.shape[:2]

N = 60
BUF = np.zeros((YY, XX, N), np.uint8)
iN = 0

for ii in range(roi_start-N,roi_start):
   I_VIS = cv2.imread('input/in%06d.jpg' % ii)
   BUF[:, :, iN] = cv2.cvtColor(I_VIS, cv2.COLOR_BGR2GRAY)
   iN = iN + 1
   if (iN == N):
      iN = 0


TP = 0
FP = 0
FN = 0

for i in range (roi_start, roi_end, step):
   I_VIS = cv2.imread('input/in%06d.jpg' % i)
   I = cv2.cvtColor(I_VIS, cv2.COLOR_BGR2GRAY)
   BUF[:, :, iN] = I
   iN = iN + 1
   if (iN == N):
      iN = 0

   mediana = np.uint8(np.median(BUF, 2))

   cv2.imshow("Model tla", mediana)
   Diff = cv2.absdiff(I, mediana)
   cv2.imshow("Image", I)
   cv2.imshow("Difference", Diff)

   B = cv2.threshold(Diff,  30, 255, cv2.THRESH_BINARY)
   B = B[1]

   kernel = np.ones((3, 3), np.uint8)
   B1 = cv2.erode(B, kernel, iterations=1)
   cv2.imshow("Binary1", B1)
   kernel = np.ones((4, 4), np.uint8)
   B2 = cv2.dilate(B1, kernel, iterations=2)
   cv2.imshow("Binary2", B2)
   kernel = np.ones((2, 2), np.uint8)
   B3 = cv2.erode(B2, kernel, iterations=1)
   cv2.imshow("Binary3", B3)
   B4 = cv2.medianBlur(B3, 9)
   cv2.imshow("Binary4", B4)
   cv2.waitKey(10)
   I_prev = I


   retval, labels, stats, centroids = cv2.connectedComponentsWithStats(B4)
   # retval -- liczba znalezionych grup pikseli
   # labels -- obraz z indeksami
   # stats -- lewy skrajny punkt, gorny skrajny punkt, szerokosc, wysokosc, pole
   # centroids -- srodki ciezkosci

   cv2.imshow("Labels", np.uint8(labels / stats.shape[0] * 255))
   if (stats.shape[0] > 1):  # czy sa jakies obiekty
      tab = stats[1:, 4]  # wyciecie 4 kolumny bez pierwszego elementu
      pi = np.argmax(tab)  # znalezienie indeksu najwiekszego elementu
      pi = pi + 1  # inkrementacja bo chcemy indeks w stats, a nie w tab
      # wyrysownie bbox
      cv2.rectangle(I_VIS, (stats[pi, 0], stats[pi, 1]), (stats[pi, 0] + stats[pi, 2], stats[pi, 1] + stats[pi, 3]), (255, 0, 0), 2)
      # wypisanie informacji o polu i numerze najwiekszego elementu
      cv2.putText(I_VIS, "%f" % stats[pi, 4], (stats[pi, 0], stats[pi, 1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
      cv2.putText(I_VIS, "%d" % pi, (np.int(centroids[pi, 0]), np.int(centroids[pi, 1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0))
      cv2.imshow("I_VIS", I_VIS)
   
   GTB = cv2.imread('groundtruth/gt%06d.png' % i)
   GTB = cv2.cvtColor(GTB, cv2.COLOR_BGR2GRAY)
   cv2.imshow("GTB", GTB)

   TP_M = np.logical_and((B4 == 255),(GTB == 255)) # iloczyn logiczny odpowiednich elementow macierzy
   TP_S = np.sum(TP_M) # suma elementow w macierzy
   TP = TP + TP_S # aktualizacja wskaznika globalnego

   FP_M = np.logical_and((B4 == 255),(GTB == 0)) # iloczyn logiczny odpowiednich elementow macierzy
   FP_S = np.sum(FP_M) # suma elementow w macierzy
   FP = FP + FP_S # aktualizacja wskaznika globalnego

   FN_M = np.logical_and((B4 == 0),(GTB == 255)) # iloczyn logiczny odpowiednich elementow macierzy
   FN_S = np.sum(FN_M) # suma elementow w macierzy
   FN = FN + FN_S # aktualizacja wskaznika globalnego


P = TP / (TP + FP)
R = TP / (TP + FN)
F1 = 2*P*R / (P + R)
print(F1)
