import matplotlib.pyplot as plt
import cv2
import numpy as np

def hanning2D(n):
    h = np.hanning(n)
    return np.sqrt(np.outer(h, h))

def highpassFilter(size):
    rows = np.cos(np.pi*np.matrix([-0.5 + x/(size[0]-1) for x in range(size[0])]))
    cols = np.cos(np.pi*np.matrix([-0.5 + x/(size[1]-1) for x in range(size[1])]))
    X = np.outer(rows, cols)
    return (1.0 - X) * (2.0 - X)

wzor = cv2.imread('obrazy_Mellin/domek_r0_64.pgm')
wzor_in = cv2.cvtColor(wzor, cv2.COLOR_BGR2GRAY)

for l in range(0, 330, 30):
    nazwa_ze_sciezka = 'obrazy_Mellin/domek_r' + str(l) + '.pgm'
    J = cv2.imread(nazwa_ze_sciezka)
    J_in = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)


    # J = cv2.imread('obrazy_Mellin/domek_r30.pgm')
    # J_in = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)


    wzor_2 = np.zeros(J_in.shape)
    wzor_han = hanning2D(wzor_in.shape[0])
    wzor = wzor_in*wzor_han
    wzor_2[0:wzor.shape[0], 0:wzor.shape[1]] = wzor

    J_fft = np.fft.fft2(J_in)
    J_fft = np.fft.fftshift(J_fft)
    wzor_fft = np.fft.fft2(wzor_2)
    wzor_fft = np.fft.fftshift(wzor_fft)

    J_filtr = highpassFilter(J_fft.shape)
    J_filtered = np.abs(J_fft)*J_filtr
    wzor_filtr = highpassFilter(wzor_fft.shape)
    wzor_filtered = np.abs(wzor_fft)*wzor_filtr

    J_fft = np.abs(J_filtered)
    wzor_fft = np.abs(wzor_filtered)


    #R = wzor_fft.shape[0]//2
    M = wzor_fft.shape[0]/np.log(wzor_fft.shape[0]//2)
    center = (wzor_fft.shape[0]//2, wzor_fft.shape[1]//2)
    wzor_logpolar = cv2.logPolar(wzor_fft, center, M, cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)
    J_logpolar = cv2.logPolar(J_fft, center, M, cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)


    J_fft = np.fft.fft2(J_logpolar)
    wzor_fft = np.fft.fft2(wzor_logpolar)

    sprzezenie = np.conj(wzor_fft) * J_fft
    sprzezenie = sprzezenie / np.abs(sprzezenie)
    corr = np.abs(np.fft.ifft2(sprzezenie))

    wsp_kata, wsp_logr = np.unravel_index(np.argmax(corr), corr.shape)
    rozmiar_logr = J_logpolar.shape[0]
    rozmiar_kata = J_logpolar.shape[1]

    if wsp_logr > rozmiar_logr//2:
        wykl = rozmiar_logr - wsp_logr #powiekszenie
    else:
        wykl = - wsp_logr #pomniejszenie

    A = (wsp_kata * 360.0) / rozmiar_kata
    kat1 = - A
    kat2 = 180 - A

    skala = np.exp(wykl/M)  # gdzie M to parametr funkcji cv2.logPolar
    print(skala)

    im = np.zeros(J_in.shape)
    x_1 = int((J_in.shape[0]-wzor_in.shape[0])/2)
    x_2 = int((J_in.shape[0]+wzor_in.shape[0])/2)
    y_1 = int((J_in.shape[1]-wzor_in.shape[1])/2)
    y_2 = int((J_in.shape[1]+wzor_in.shape[1])/2)
    im[x_1:x_2, y_1:y_2] = wzor_in


    srodekTrans = (im.shape[0] / 2 - 0.5, im.shape[1] / 2 - 0.5)
    # im to obraz wzorca uzupelniony zerami, ale ze wzorcem umieszczonym na srodku, a nie w lewym, gornym rogu!
    macierz_translacji_1 = cv2.getRotationMatrix2D(srodekTrans, kat1, skala)
    obraz_obrocony_przeskalowany_1 = cv2.warpAffine(im, macierz_translacji_1, im.shape)

    macierz_translacji_2 = cv2.getRotationMatrix2D(srodekTrans, kat2, skala)
    obraz_obrocony_przeskalowany_2 = cv2.warpAffine(im, macierz_translacji_2, im.shape)

    obraz_fft_1 = np.fft.fft2(obraz_obrocony_przeskalowany_1)
    obraz_fft_2 = np.fft.fft2(obraz_obrocony_przeskalowany_2)
    J_fft = np.fft.fft2(J_in)

    sprzezenie_1 = np.conj(obraz_fft_1) * J_fft
    sprzezenie_1 = sprzezenie_1 / np.abs(sprzezenie_1)
    corr_1 = np.abs(np.fft.ifft2(sprzezenie_1))

    sprzezenie_2 = np.conj(obraz_fft_2) * J_fft
    sprzezenie_2 = sprzezenie_2 / np.abs(sprzezenie_2)
    corr_2 = np.abs(np.fft.ifft2(sprzezenie_2))

    if (np.amax(corr_1)>np.amax(corr_2)):
        corr_lepszy = corr_1
        wzor_lepszy = obraz_obrocony_przeskalowany_1
    else:
        corr_lepszy = corr_2
        wzor_lepszy = obraz_obrocony_przeskalowany_2

    dy, dx = np.unravel_index(np.argmax(corr_lepszy), corr_lepszy.shape)
    if dx > J_in.shape[0] - 5:
        dx = dx - J_in.shape[0]
    print(dx, dy)
    macierz_translacji = np.float32([[1, 0, dx], [0, 1, dy]]) # gdzie dx, dy - wektor przesuniecia
    obraz_przesuniety = cv2.warpAffine(wzor_lepszy, macierz_translacji, (J_in.shape[1], J_in.shape[0]))


    plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plt.gray()
    plt.imshow(obraz_przesuniety)
    plt.title('Wzorzec - obrocony i przeskalowany')

    # plt.figure()
    plt.subplot(1, 2, 2)
    plt.gray()
    plt.imshow(J_in)
    plt.title('Obraz przeszukiwany')

for l in range(10, 80, 10):
    nazwa_ze_sciezka = 'obrazy_Mellin/domek_s' + str(l) + '.pgm'
    J = cv2.imread(nazwa_ze_sciezka)
    J_in = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)


    # J = cv2.imread('obrazy_Mellin/domek_r30.pgm')
    # J_in = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)


    wzor_2 = np.zeros(J_in.shape)
    wzor_han = hanning2D(wzor_in.shape[0])
    wzor = wzor_in*wzor_han
    wzor_2[0:wzor.shape[0], 0:wzor.shape[1]] = wzor

    J_fft = np.fft.fft2(J_in)
    J_fft = np.fft.fftshift(J_fft)
    wzor_fft = np.fft.fft2(wzor_2)
    wzor_fft = np.fft.fftshift(wzor_fft)

    J_filtr = highpassFilter(J_fft.shape)
    J_filtered = np.abs(J_fft)*J_filtr
    wzor_filtr = highpassFilter(wzor_fft.shape)
    wzor_filtered = np.abs(wzor_fft)*wzor_filtr

    J_fft = np.abs(J_filtered)
    wzor_fft = np.abs(wzor_filtered)


    #R = wzor_fft.shape[0]//2
    M = wzor_fft.shape[0]/np.log(wzor_fft.shape[0]//2)
    center = (wzor_fft.shape[0]//2, wzor_fft.shape[1]//2)
    wzor_logpolar = cv2.logPolar(wzor_fft, center, M, cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)
    J_logpolar = cv2.logPolar(J_fft, center, M, cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)


    J_fft = np.fft.fft2(J_logpolar)
    wzor_fft = np.fft.fft2(wzor_logpolar)

    sprzezenie = np.conj(wzor_fft) * J_fft
    sprzezenie = sprzezenie / np.abs(sprzezenie)
    corr = np.abs(np.fft.ifft2(sprzezenie))

    wsp_kata, wsp_logr = np.unravel_index(np.argmax(corr), corr.shape)
    rozmiar_logr = J_logpolar.shape[0]
    rozmiar_kata = J_logpolar.shape[1]

    if wsp_logr > rozmiar_logr//2:
        wykl = rozmiar_logr - wsp_logr #powiekszenie
    else:
        wykl = - wsp_logr #pomniejszenie

    A = (wsp_kata * 360.0) / rozmiar_kata
    kat1 = - A
    kat2 = 180 - A

    skala = np.exp(wykl/M)  # gdzie M to parametr funkcji cv2.logPolar
    print(skala)

    im = np.zeros(J_in.shape)
    x_1 = int((J_in.shape[0]-wzor_in.shape[0])/2)
    x_2 = int((J_in.shape[0]+wzor_in.shape[0])/2)
    y_1 = int((J_in.shape[1]-wzor_in.shape[1])/2)
    y_2 = int((J_in.shape[1]+wzor_in.shape[1])/2)
    im[x_1:x_2, y_1:y_2] = wzor_in


    srodekTrans = (im.shape[0] / 2 - 0.5, im.shape[1] / 2 - 0.5)
    # im to obraz wzorca uzupelniony zerami, ale ze wzorcem umieszczonym na srodku, a nie w lewym, gornym rogu!
    macierz_translacji_1 = cv2.getRotationMatrix2D(srodekTrans, kat1, skala)
    obraz_obrocony_przeskalowany_1 = cv2.warpAffine(im, macierz_translacji_1, im.shape)

    macierz_translacji_2 = cv2.getRotationMatrix2D(srodekTrans, kat2, skala)
    obraz_obrocony_przeskalowany_2 = cv2.warpAffine(im, macierz_translacji_2, im.shape)

    obraz_fft_1 = np.fft.fft2(obraz_obrocony_przeskalowany_1)
    obraz_fft_2 = np.fft.fft2(obraz_obrocony_przeskalowany_2)
    J_fft = np.fft.fft2(J_in)

    sprzezenie_1 = np.conj(obraz_fft_1) * J_fft
    sprzezenie_1 = sprzezenie_1 / np.abs(sprzezenie_1)
    corr_1 = np.abs(np.fft.ifft2(sprzezenie_1))

    sprzezenie_2 = np.conj(obraz_fft_2) * J_fft
    sprzezenie_2 = sprzezenie_2 / np.abs(sprzezenie_2)
    corr_2 = np.abs(np.fft.ifft2(sprzezenie_2))

    if (np.amax(corr_1)>np.amax(corr_2)):
        corr_lepszy = corr_1
        wzor_lepszy = obraz_obrocony_przeskalowany_1
    else:
        corr_lepszy = corr_2
        wzor_lepszy = obraz_obrocony_przeskalowany_2

    dy, dx = np.unravel_index(np.argmax(corr_lepszy), corr_lepszy.shape)
    dy = dy - J_in.shape[1]
    print(dx, dy)
    macierz_translacji = np.float32([[1, 0, dx], [0, 1, dy]]) # gdzie dx, dy - wektor przesuniecia
    obraz_przesuniety = cv2.warpAffine(wzor_lepszy, macierz_translacji, (J_in.shape[1], J_in.shape[0]))


    plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plt.gray()
    plt.imshow(obraz_przesuniety)
    plt.title('Wzorzec - obrocony i przeskalowany')

    # plt.figure()
    plt.subplot(1, 2, 2)
    plt.gray()
    plt.imshow(J_in)
    plt.title('Obraz przeszukiwany')


plt.show()


#ta druga koleracja powinna dać punkt (0, 0) ?
#w drugim furierze juz nie robimy shifta, tylko przed logpolarem

#stare złe i nie działa
# macierz_translacji = cv2.getRotationMatrix2D((srodekTrans[0], srodekTrans[1]), kat, skala) gdzie srodekTrans to srodek obrazu:
# srodekTrans = [math.floor((obraz.shape[0] + 1) / 2), math.floor((obraz.shape[1] + 1 ) / 2)]
