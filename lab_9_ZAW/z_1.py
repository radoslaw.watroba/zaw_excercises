import matplotlib.pyplot as plt
import cv2
import numpy as np

wzor = cv2.imread('obrazy_Mellin/wzor.pgm')
wzor = cv2.cvtColor(wzor, cv2.COLOR_BGR2GRAY)

J = cv2.imread('obrazy_Mellin/domek_r0.pgm')
J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)

wzor_2 = np.zeros(J.shape)
wzor_2[0:wzor.shape[0], 0:wzor.shape[1]] = wzor

J_fft = np.fft.fft2(J)
wzor_fft = np.fft.fft2(wzor_2)
sprzezenie = np.conj(wzor_fft) * J_fft
sprzezenie = sprzezenie / np.abs(sprzezenie)
corr = np.abs(np.fft.ifft2(sprzezenie))

dy, dx = np.unravel_index(np.argmax(corr), corr.shape)
macierz_translacji = np.float32([[1, 0, dx], [0, 1, dy]]) # gdzie dx, dy - wektor przesuniecia
obraz_przesuniety = cv2.warpAffine(wzor, macierz_translacji, (J.shape[1], J.shape[0]))

plt.figure()
plt.gray()
plt.imshow(J)
plt.plot(dx, dy, '*m')
plt.title('Domki')

plt.figure()
plt.gray()
plt.imshow(obraz_przesuniety)
plt.title('Obraz przesuniety')
plt.show()

