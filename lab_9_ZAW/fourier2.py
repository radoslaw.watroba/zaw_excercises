import matplotlib.pyplot as plt
import cv2
import numpy as np


def hanning2D(n):
    h = np.hanning(n)
    return np.sqrt(np.outer(h,h))


def highpassFilter(size):
    rows = np.cos(np.pi*np.matrix([-0.5 + x/(size[0]-1) for x in range (size[0])]))
    cols = np.cos(np.pi*np.matrix([-0.5 + x/(size[1]-1) for x in range(size[1])]))
    X = np.outer(rows,cols)
    return (1.0 - X)*(2.0 - X)


pattern_raw = cv2.imread('obrazy_Mellin/domek_r0_64.pgm')
pattern_raw = cv2.cvtColor(pattern_raw, cv2.COLOR_RGB2GRAY)

I = cv2.imread('obrazy_Mellin/domek_r30.pgm')
I = cv2.cvtColor(I, cv2.COLOR_RGB2GRAY)

pattern_hanning = hanning2D(pattern_raw.shape[0])
pattern = pattern_raw*pattern_hanning

#uzupelnienie zerami
small_pattern = pattern
pattern = np.zeros(I.shape)
pattern[0:small_pattern.shape[0], 0:small_pattern.shape[1]] = small_pattern

pattern_fft = np.fft.fft2(pattern)
image_fft = np.fft.fft2(I)
image_fft = np.fft.fftshift(image_fft)
pattern_fft = np.fft.fftshift(pattern_fft)

filter_pattern = highpassFilter(pattern_fft.shape)
filter_image = highpassFilter(image_fft.shape)
filtered_pattern = filter_pattern*np.abs(pattern_fft)
filtered_image = filter_image*np.abs(image_fft)


#logPolar
size = filtered_image.shape
M = filtered_image.shape[0]/np.log(filtered_image.shape[0]//2)
center = (size[0]/2, size[1]/2)
logPolar_image = cv2.logPolar(filtered_image, center, M, flags= cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)

size = filtered_pattern.shape
M = filtered_pattern.shape[0]/np.log(filtered_pattern.shape[0]//2)
center = (size[0]/2, size[1]/2)
logPolar_pattern = cv2.logPolar(filtered_pattern, center, M, flags= cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)

#
# plt.figure()
# plt.gray()
# plt.imshow(logPolar_image)
# plt.title('J_logpolar')
#
# plt.figure()
# plt.gray()
# plt.imshow(logPolar_pattern)
# plt.title('wzor_logpolar')
# plt.show()



#FFT logpolar
pattern_fft = np.fft.fft2(logPolar_pattern)
image_fft = np.fft.fft2(logPolar_image)
R = np.conj(pattern_fft)*image_fft/np.abs(np.conj(pattern_fft)*image_fft)
cor = np.abs(np.fft.ifft2(R))

wsp_kat, wsp_logr = np.unravel_index(np.argmax(cor), cor.shape)

rozmiar_logr = logPolar_pattern.shape[0]

if wsp_logr >  rozmiar_logr//2:
    wyk1 = rozmiar_logr-wsp_logr
else:
    wyk1 = -wsp_logr

rozmiar_kat = cor.shape[0]
A =(wsp_kat*360)/rozmiar_kat
kat1 = -A
kat2 = 180-A

skala = np.exp(wyk1/M)   #   gdzie M to parametr funkcji cv2. logPolar, a wykl wyliczamy jako:

print(skala)
#uzupelnienie zerami

small_pattern = pattern_raw
im = np.zeros(I.shape)
im_sh = im.shape
im[int(im_sh[0]/2-small_pattern.shape[0]/2):int(im_sh[0]/2+small_pattern.shape[0]/2),
   int(im_sh[1]/2-small_pattern.shape[1]/2):int(im_sh[1]/2+small_pattern.shape[1]/2)] = small_pattern


srodekTrans = (im.shape[0] / 2 - 0.5, im.shape[1]/ 2 - 0.5)
macierz_translacji = cv2.getRotationMatrix2D(srodekTrans, kat1, skala) # gdzie srodekTrans mozna wyliczyc jako:
obraz_obrocony_przeskalowany1 = cv2.warpAffine(im, macierz_translacji, im.shape)
# im to obraz wzorca uzupelniony zerami, ale ze wzorcem umieszczonym na srodku, a nie w lewym, gornym rogu!
macierz_translacji = cv2.getRotationMatrix2D(srodekTrans, kat2, skala) # gdzie srodekTrans mozna wyliczyc jako:
obraz_obrocony_przeskalowany2 = cv2.warpAffine(im, macierz_translacji, im.shape)

image_fft = np.fft.fft2(I)
pattern1_fft = np.fft.fft2(obraz_obrocony_przeskalowany1)
R1 = np.conj(pattern1_fft)*image_fft/np.abs(np.conj(pattern1_fft)*image_fft)
cor1 = np.abs(np.fft.ifft2(R1))

pattern2_fft = np.fft.fft2(obraz_obrocony_przeskalowany2)
R2 = np.conj(pattern2_fft)*image_fft/np.abs(np.conj(pattern2_fft)*image_fft)
cor2 = np.abs(np.fft.ifft2(R2))

#print(np.amax(cor1),np.amax(cor2))
#wybór lepszego kata
if (np.amax(cor1)>np.amax(cor2)):
    cor = cor1
    pattern = obraz_obrocony_przeskalowany1
else:
    cor = cor2
    pattern = obraz_obrocony_przeskalowany2

y, x = np.unravel_index(np.argmax(pattern), pattern.shape)

print(y, x)

# to nie dziala :(
translation_matrix = np.float32([[1,0,x],[0,1,y]])
translated_pattern = cv2.warpAffine(pattern, translation_matrix, (pattern.shape[0], pattern.shape[1]))

plt.figure()
plt.imshow(I)
plt.plot(x, y, '*', color='r')
plt.gray()
plt.figure()
plt.imshow(pattern)
plt.gray()
plt.figure()
plt.imshow(translated_pattern)
plt.gray()
plt.show()
