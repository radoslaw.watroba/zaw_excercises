import matplotlib.pyplot as plt
import cv2
import numpy as np
import scipy.ndimage.filters as filters
from scipy.spatial import distance
import pm

def find_max(image, size, threshold): #size - rozmiar maski filtra maksymalnego
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)


def Harris(image, mask_size):
    k = 0.05

    sobel_1_x = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=mask_size)
    sobel_1_y = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=mask_size)
    sobel_1_x2 = sobel_1_x * sobel_1_x
    sobel_1_y2 = sobel_1_y * sobel_1_y
    sobel_1_xy = sobel_1_x * sobel_1_y

    I_x2 = cv2.GaussianBlur(sobel_1_x2, (mask_size, mask_size), 0)
    I_xy = cv2.GaussianBlur(sobel_1_xy, (mask_size, mask_size), 0)
    I_y2 = cv2.GaussianBlur(sobel_1_y2, (mask_size, mask_size), 0)

    det_M = I_x2 * I_y2 - I_xy * I_xy
    trace_M = I_x2 + I_y2

    H = det_M - k * trace_M * trace_M
    H = cv2.normalize(H, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    return H

def descriptor(I, pts, n_size):
    if n_size % 2 != 0:
        n_size = n_size + 1
    Y = I.shape[0]
    X = I.shape[1]
    l_otoczen = []
    l_wspolrzednych = []
    pts = list(filter(lambda pt: pt[0] >= n_size/2 and pt[0] < Y - n_size/2 and pt[1] >= n_size/2 and pt[1] < X - n_size/2, zip(pts[0], pts[1])))
    for i in range(len(pts)):
        opis = (I[pts[i][0]-int(n_size/2):pts[i][0]+int(n_size/2), pts[i][1]-int(n_size/2):pts[i][1]+int(n_size/2)]).flatten()
        opis_aff = (opis - np.mean(opis)) / np.std(opis)
        l_otoczen.append(opis_aff)
        l_wspolrzednych.append(pts[i])

    wynik_funkcji = list(zip(l_otoczen, l_wspolrzednych))
    return wynik_funkcji

def compare(desc_1, desc_2, n, otoczenie):
    if otoczenie % 2 != 0:
        otoczenie = otoczenie + 1
    lst = []
    for i_1 in range(len(desc_1)):
        for i_2 in range(len(desc_2)):
            tmp = desc_2[i_2][0] - desc_1[i_1][0]
            wynik = np.sum(np.power(tmp, 2))
            lst.append([[i_1, i_2], wynik])

            #dists = []
            #print(i_2[0])
            #lst.append((i_1[1], i_2[1], distance.euclidean(i_1[0], i_2[0])))
            #lst.append((i_1[1], i_2[1], sum(sum(distance.cdist(i_1[0], i_2[0], metric='euclidean')))))
            #lst.append((i_1[1], i_2[1], sum(cv2.absdiff(i_1[0], i_2[0]))))
            # for iter in range(0, otoczenie*otoczenie):
            #     dist = distance.euclidean(i_1[0][iter], i_2[0][iter])
            #     dists.append(dist)
            # lst.append((i_1[1], i_2[1], sum(dists)))
    lst.sort(key=lambda x: x[1], reverse=False)
    return lst[0:n]


I_1_org = cv2.imread('fontanna1.jpg')
I_1 = cv2.cvtColor(I_1_org, cv2.COLOR_BGR2GRAY)
I_2_org = cv2.imread('fontanna2.jpg')
I_2 = cv2.cvtColor(I_2_org, cv2.COLOR_BGR2GRAY)




mask_size = 5
H_1 = Harris(I_1, mask_size)
maxim_1 = find_max(H_1, mask_size, 0.5)

H_2 = Harris(I_2, mask_size)
maxim_2 = find_max(H_2, mask_size, 0.5)

y = max(I_1.shape[0], I_2.shape[0])
x = max(I_1.shape[1], I_2.shape[1])
Blank_1 = np.zeros((y, x), dtype=int)
Blank_1[0:I_1.shape[0], 0:I_1.shape[1]] = I_1
Blank_2 = np.zeros((y, x), dtype=int)
Blank_2[0:I_2.shape[0], 0:I_2.shape[1]] = I_2
I_1 = Blank_1
I_2 = Blank_2

otoczenie = 15
desc_1 = descriptor(I_1, maxim_1, otoczenie)
desc_2 = descriptor(I_2, maxim_2, otoczenie)

lista_pkt = compare(desc_1, desc_2, 20, otoczenie)

plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.imshow(I_1)
#plt.imshow(cv2.cvtColor(I_1, cv2.COLOR_BGR2RGB))
plt.plot(maxim_1[1], maxim_1[0], '*', color='m')
plt.title('Zdjecie 1')

plt.subplot(1, 2, 2)
plt.imshow(I_2)
#plt.imshow(cv2.cvtColor(I_2, cv2.COLOR_BGR2RGB))
plt.plot(maxim_2[1], maxim_2[0], '*', color='m')
plt.title('Zdjecie 2')
plt.show()

lista = []
n = 20
for i in range(n):
    xy1 = desc_1[lista_pkt[i][0][0]][1]
    xy2 = desc_2[lista_pkt[i][0][1]][1]
    lista.append([[xy1[0], xy1[1]], [xy2[0], xy2[1]]])

pm.plot_matches(I_1_org, I_2_org, lista)
plt.gray()
plt.show()

