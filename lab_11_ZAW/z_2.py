import matplotlib.pyplot as plt
import cv2
import numpy as np

SIFT = cv2.xfeatures2d.SIFT_create()

##fontanna
I_1 = cv2.imread('fontanna1.jpg')
I_2 = cv2.imread('fontanna2.jpg')

I_1_pts, I_1_descs = SIFT.detectAndCompute(I_1, None)
I_2_pts, I_2_descs = SIFT.detectAndCompute(I_2, None)

bf = cv2.BFMatcher()
matches = bf.knnMatch(I_1_descs, I_2_descs, k=2)
best_matches = [[m] for m, n in matches if m.distance < 0.2 * n.distance]

plt.figure(1)
wynik = cv2.drawMatchesKnn(I_1, I_1_pts, I_2, I_2_pts, best_matches, None, flags=2)
plt.imshow(cv2.cvtColor(wynik, cv2.COLOR_BGR2RGB))

##fontanna_2
I_1 = cv2.imread('fontanna1.jpg')
I_2 = cv2.imread('fontanna_pow.jpg')

I_1_pts, I_1_descs = SIFT.detectAndCompute(I_1, None)
I_2_pts, I_2_descs = SIFT.detectAndCompute(I_2, None)

bf = cv2.BFMatcher()
matches = bf.knnMatch(I_1_descs, I_2_descs, k=2)
best_matches = [[m] for m, n in matches if m.distance < 0.2 * n.distance]

plt.figure(2)
wynik = cv2.drawMatchesKnn(I_1, I_1_pts, I_2, I_2_pts, best_matches, None, flags=2)
plt.imshow(cv2.cvtColor(wynik, cv2.COLOR_BGR2RGB))

##budynek
I_1 = cv2.imread('budynek1.jpg')
I_2 = cv2.imread('budynek2.jpg')

I_1_pts, I_1_descs = SIFT.detectAndCompute(I_1, None)
I_2_pts, I_2_descs = SIFT.detectAndCompute(I_2, None)

bf = cv2.BFMatcher()
matches = bf.knnMatch(I_1_descs, I_2_descs, k=2)
best_matches = [[m] for m, n in matches if m.distance < 0.2 * n.distance]

plt.figure(3)
wynik = cv2.drawMatchesKnn(I_1, I_1_pts, I_2, I_2_pts, best_matches, None, flags=2)
plt.imshow(cv2.cvtColor(wynik, cv2.COLOR_BGR2RGB))

##eiffel
I_1 = cv2.imread('eiffel1.jpg')
I_2 = cv2.imread('eiffel2.jpg')

I_1_pts, I_1_descs = SIFT.detectAndCompute(I_1, None)
I_2_pts, I_2_descs = SIFT.detectAndCompute(I_2, None)

bf = cv2.BFMatcher()
matches = bf.knnMatch(I_1_descs, I_2_descs, k=2)
best_matches = [[m] for m, n in matches if m.distance < 0.5 * n.distance]

plt.figure(4)
wynik = cv2.drawMatchesKnn(I_1, I_1_pts, I_2, I_2_pts, best_matches, None, flags=2)
plt.imshow(cv2.cvtColor(wynik, cv2.COLOR_BGR2RGB))
plt.show()