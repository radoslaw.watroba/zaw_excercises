import matplotlib.pyplot as plt
import cv2
import numpy as np
import z_1
from sklearn import svm

l_prob = 600
HOG_data = np.zeros([2*l_prob, 3781], np.float32)

for i in range(0, l_prob):
    IP = cv2.imread('pos/per%05d.ppm' % (i+1))
    IN = cv2.imread('neg/neg%05d.png' % (i+1))
    F = z_1.HOG(IP)
    HOG_data[i, 0] = 1
    HOG_data[i, 1:] = F
    F = z_1.HOG(IN)
    HOG_data[i+l_prob, 0] = 0
    HOG_data[i+l_prob, 1:] = F

labels = HOG_data[:, 0]
data = HOG_data[:, 1:]
clf = svm.SVC(kernel='linear', C=1.0)
clf.fit(data, labels)
lp = clf.predict(data)

TP = 0
TN = 0
FP = 0
FN = 0

for i in range(0, len(labels)):
    if labels[i] == 1 and lp[i] == 1:
        TP += 1
    elif labels[i] == 0 and lp[i] == 0:
        TN += 1
    elif labels[i] == 0 and lp[i] == 1:
        FP += 1
    else:
        FN += 1

ACC = (TP + TN) / len(labels)
print('Dokładność wynosi:', ACC)

G = cv2.imread('testImage4.png')
G = cv2.cvtColor(G, cv2.COLOR_BGR2RGB)
G_copy = G.copy()
scale = [1.3, 1.2, 0.8, 0.6, 0.4]
for s in scale:
    img = cv2.resize(G, None, fx=s, fy=s)
    # img_copy = img.copy()
    for i in range(0, int(img.shape[1]-64), 16):
        for j in range(0, int(img.shape[0]-128), 16):
            print(i, j)
            img_piece = img[j:128+j, i:64+i]
            F = z_1.HOG(img_piece)
            lp = clf.predict([F])
            if lp == 1:
                #lewy gorny; prawy dolny
                cv2.rectangle(G_copy, (int(i/s), int(j/s)), (int((i+64)/s), int((j+128)/s)), (255, 20, 147), 2)

#cv2.rectangle(G_copy, (100, 100), (164, 228), (255, 20, 147), 2)
plt.figure()
plt.imshow(G_copy)
plt.savefig('Detekcja_4.png')
plt.show()
