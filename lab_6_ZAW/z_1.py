import cv2
import numpy as np
import matplotlib.pyplot as plt

cap = cv2.VideoCapture('vid1_IR.avi')
while(cap.isOpened()):
    ret, frame = cap.read()
    G = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    B = cv2.threshold(G, 40, 255, cv2.THRESH_BINARY)
    B = B[1]
    kernel = np.ones((3, 3), np.uint8)
    B1 = cv2.erode(B, kernel, iterations=1)
    cv2.imshow("Binary1", B1)
    kernel = np.ones((4, 4), np.uint8)
    B2 = cv2.dilate(B1, kernel, iterations=2)
    cv2.imshow("Binary2", B2)
    kernel = np.ones((2, 2), np.uint8)
    B3 = cv2.erode(B2, kernel, iterations=1)
    cv2.imshow("Binary3", B3)
    B4 = cv2.medianBlur(B3, 9)
    cv2.imshow("Binary4", B4)


    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(B4)
    # retval -- liczba znalezionych grup pikseli
    # labels -- obraz z indeksami
    # stats -- lewy skrajny punkt, gorny skrajny punkt, szerokosc, wysokosc, pole
    # centroids -- srodki ciezkosci

    cv2.imshow("Labels", np.uint8(labels / stats.shape[0] * 255))
    if (stats.shape[0] > 1):  # czy sa jakies obiekty
        #tab = stats[1:, 4]  # wyciecie 4 kolumny bez pierwszego elementu
        # wyrysownie bbox
        #stats, weights = cv2.groupRectangles(list(stats), 20) #nie dziala wiesza windowsa

        for pi in range(0, stats.shape[0]):
            if stats[pi, 3] > 0.5*stats[pi, 2]:
                #rozpartujemy bloczki najwieksze mozliwe >200
                if stats[pi, 4] > 900:
                    lewy = stats[pi, 0]
                    prawy = stats[pi, 0] + stats[pi, 2]
                    gorny = stats[pi, 1]
                    dolny = stats[pi, 1] + stats[pi, 3]
                    #wys = stats[pi, 3]
                    #szer = stats[pi, 2]
                    #pole = wys*szer

                    for pi_w in range(0, stats.shape[0]):
                        lewy_w = stats[pi_w, 0]
                        prawy_w = stats[pi_w, 0] + stats[pi_w, 2]
                        gorny_w = stats[pi_w, 1]
                        dolny_w = stats[pi_w, 1] + stats[pi_w, 3]
                        #wys_w = stats[pi_w, 3]
                        szer_w = stats[pi_w, 2]
                        if stats[pi_w, 3] > stats[pi_w, 2]:
                           # dolaczamy bloczki wieksze niz 40
                           if stats[pi_w, 4] > 10: # and stats[pi_w, 4] < 1400:
                               if (lewy-5 <= (lewy_w + (1/2)*szer_w) <= prawy+5 and gorny_w < gorny - 10) or (lewy <= (lewy_w + (1/2)*szer_w) <= prawy and dolny_w > dolny + 10):
                                   lewy = min(lewy, lewy_w)
                                   gorny = min(gorny, gorny_w)
                                   prawy = max(prawy, prawy_w)
                                   dolny = max(dolny, dolny_w)
                                   pole = (dolny - gorny) * (prawy - lewy)
                    
                    cv2.rectangle(G, (lewy, gorny), (prawy, dolny), (255, 0, 0), 2)
                    # wypisanie informacji o polu i numerze elementow
                    #cv2.putText(G, "%f" % pole, (lewy, gorny), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 0, 0))
                    #cv2.putText(G, "%d" % pi, (np.int(centroids[pi, 0]), np.int(centroids[pi, 1])), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0))
        cv2.imshow("G", G)
    cv2.waitKey(10)
    if cv2.waitKey(1) & 0xFF == ord('q'): # przerwanie petli po wcisnieciu klawisza ’q’
        break

cap.release()