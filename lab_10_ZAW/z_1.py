import matplotlib.pyplot as plt
import cv2
import numpy as np
import scipy.ndimage.filters as filters


def find_max(image, size, threshold): #size - rozmiar maski filtra maksymalnego
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)


def Harris(image, mask_size):
    k = 0.05

    sobel_1_x = cv2.Sobel(image, cv2.CV_32F, 1, 0, ksize=mask_size)
    sobel_1_y = cv2.Sobel(image, cv2.CV_32F, 0, 1, ksize=mask_size)
    sobel_1_x2 = sobel_1_x * sobel_1_x
    sobel_1_y2 = sobel_1_y * sobel_1_y
    sobel_1_xy = sobel_1_x * sobel_1_y

    I_x2 = cv2.GaussianBlur(sobel_1_x2, (mask_size, mask_size), 0)
    I_xy = cv2.GaussianBlur(sobel_1_xy, (mask_size, mask_size), 0)
    I_y2 = cv2.GaussianBlur(sobel_1_y2, (mask_size, mask_size), 0)

    det_M = I_x2 * I_y2 - I_xy * I_xy
    trace_M = I_x2 + I_y2

    H = det_M - k * trace_M * trace_M
    H = cv2.normalize(H, None, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_32F)
    return H

I_1 = cv2.imread('fontanna1.jpg')
I_2 = cv2.imread('fontanna2.jpg')

mask_size = 7
H_1 = Harris(I_1, mask_size)
maxim_1 = find_max(H_1, mask_size, 0.5)

H_2 = Harris(I_2, mask_size)
maxim_2 = find_max(H_2, mask_size, 0.5)

print(maxim_1)

plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.imshow(cv2.cvtColor(I_1, cv2.COLOR_BGR2RGB))
plt.plot(maxim_1[1], maxim_1[0], '*', color='m')
plt.title('Fontanna 1')

plt.subplot(1, 2, 2)
plt.imshow(cv2.cvtColor(I_2, cv2.COLOR_BGR2RGB))
plt.plot(maxim_2[1], maxim_2[0], '*', color='m')
plt.title('Fontanna 2')
#plt.show()

I_1 = cv2.imread('budynek1.jpg')
I_2 = cv2.imread('budynek2.jpg')

mask_size = 7
H_1 = Harris(I_1, mask_size)
maxim_1 = find_max(H_1, mask_size, 0.5)

H_2 = Harris(I_2, mask_size)
maxim_2 = find_max(H_2, mask_size, 0.5)

print(maxim_1)

plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.imshow(cv2.cvtColor(I_1, cv2.COLOR_BGR2RGB))
plt.plot(maxim_1[1], maxim_1[0], '*', color='m')
plt.title('Budynek 1')

plt.subplot(1, 2, 2)
plt.imshow(cv2.cvtColor(I_2, cv2.COLOR_BGR2RGB))
plt.plot(maxim_2[1], maxim_2[0], '*', color='m')
plt.title('Budynek 2')
plt.show()


I_1 = cv2.imread('fontanna1.jpg')
I_2 = cv2.imread('fontanna_pow.jpg')

mask_size = 7
H_1 = Harris(I_1, mask_size)
maxim_1 = find_max(H_1, mask_size, 0.5)

H_2 = Harris(I_2, mask_size)
maxim_2 = find_max(H_2, mask_size, 0.5)

print(maxim_1)

plt.figure(figsize=(12, 6))
plt.subplot(1, 2, 1)
plt.imshow(cv2.cvtColor(I_1, cv2.COLOR_BGR2RGB))
plt.plot(maxim_1[1], maxim_1[0], '*', color='m')
plt.title('Fontanna 1')

plt.subplot(1, 2, 2)
plt.imshow(cv2.cvtColor(I_2, cv2.COLOR_BGR2RGB))
plt.plot(maxim_2[1], maxim_2[0], '*', color='m')
plt.title('Fontanna powiekszona')
plt.show()