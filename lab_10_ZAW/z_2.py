import matplotlib.pyplot as plt
import cv2
import numpy as np
import scipy.ndimage.filters as filters

def find_max(image, size, threshold): #size - rozmiar maski filtra maksymalnego
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)

def pyramid(image, blur_nbr, k, sigma):
    res_shape = (blur_nbr, image.shape[0], image.shape[1], 3)
    res_img = np.zeros(res_shape)
    fimage = np.float64(image)
    prev_img = cv2.GaussianBlur(fimage, (0, 0), sigmaX=sigma, sigmaY=sigma)
    for i in range(0, blur_nbr - 1):
        sigma = k*sigma
        img = cv2.GaussianBlur(prev_img, (0, 0), sigmaX=sigma, sigmaY=sigma)
        res_img[i, :, :] = img - prev_img
        prev_img = img

    return res_img


I_1 = cv2.imread('fontanna1.jpg')
I_pow = cv2.imread('fontanna_pow.jpg')
mask_size = 7

pyramid_1 = pyramid(I_1, 5, 1.26, 1.6)
maxim_1 = find_max(pyramid_1, mask_size, 0.2)
x_1 = maxim_1[2] # wspolrzedne x we wszystkich skalach
y_1 = maxim_1[1]


pyramid_pow = pyramid(I_pow, 10, 1.26, 1.6)
maxim_pow = find_max(pyramid_pow, mask_size, 0.2)
x_pow = maxim_pow[2] # wspolrzedne x we wszystkich skalach
y_pow = maxim_pow[1]

plt.figure(figsize=(12, 7))
for k in range(0, pyramid_1.shape[0] - 1):
    plt.subplot(2, 3, k+1)
    plt.imshow(pyramid_1[k, :, :])
    plt.plot(x_1[maxim_1[0] == k], y_1[maxim_1[0] == k], '*', color='m')
    plt.title('Roznica nr ' + str(k+1))

plt.figure(figsize=(12, 9))
for k in range(0, pyramid_pow.shape[0] - 1):
    plt.subplot(3, 4, k + 1)
    plt.imshow(pyramid_pow[k, :, :])
    plt.plot(x_pow[maxim_pow[0] == k], y_pow[maxim_pow[0] == k], '*', color='m')
    plt.title('Roznica nr ' + str(k + 1))

plt.show()

#na duzym roznica nr 3; na malym roznica nr 8, różnią się o 5 skal